package main

import (
	"encoding/json"
	"log"
	"strconv"

	bolt "github.com/coreos/bbolt"
)

type Cliente struct {
	NroCliente int
	Nombre     string
	Apellido   string
	Domicilio  string
	Telefono   string
}

type Tarjeta struct {
	NroTarjeta   string
	NroCliente   int
	ValidaDesde  string
	ValidaHasta  string
	CodSeguridad string
	LimiteCompra float64
	Estado       string
}

type Comercio struct {
	NroComercio  int
	Nombre       string
	Domicilio    string
	CodigoPostal string
	Telefono     string
}

type Compras struct {
	NroOperacion int
	NroTarjeta   string
	NroComercio  int
	Fecha        string
	Monto        float64
	Pagado       bool
}

func CreateUpdate(db *bolt.DB, bucketName string, key []byte, val []byte) error {
	tx, err := db.Begin(true)
	if err != nil {
		return err
	}
	defer tx.Rollback()

	b, _ := tx.CreateBucketIfNotExists([]byte(bucketName))

	err = b.Put(key, val)
	if err != nil {
		return err
	}

	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}

func ReadUnique(db *bolt.DB, bucketName string, key []byte) ([]byte, error) {
	var buf []byte

	err := db.View(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte(bucketName))
		buf = b.Get(key)
		return nil
	})

	return buf, err
}

func main() {
	db, err := bolt.Open("tarjetas.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()


	//Clientes

	cliente1 := Cliente{12320, "Jorge", "Catalanes", "Callao 1415", "011147040798"} // fixme
	data, err := json.Marshal(cliente1)
	if err != nil {
		log.Fatal(err)
	}

	cliente2 := Cliente{12321, "Catalina", "Barrios", "Juan Francisco Segui 845", "011153467824"}
	data2, err := json.Marshal(cliente2)
	if err != nil {
		log.Fatal(err)
	}

	cliente3 := Cliente{12322, "Samuel", "Davile", "Nuñez 2530", "011589675423"}
	data3, err := json.Marshal(cliente3)
	if err != nil {
		log.Fatal(err)
	}

	//Tarjetas

	tarjeta1 := Tarjeta{"4539608248204879", 1, "201907", "202407", "603", 1000.00, "vigente"}
	data4, err := json.Marshal(tarjeta1)
	if err != nil {
		log.Fatal(err)
	}
	tarjeta2 := Tarjeta{"4520139769562264", 2, "201811", "202311", "623", 1222.00, "anulada"}
	data5, err := json.Marshal(tarjeta2)
	if err != nil {
		log.Fatal(err)
	}

	tarjeta3 := Tarjeta{"4017815492979764", 3, "201609", "202109", "506", 6000.00, "vigente"}
	data6, err := json.Marshal(tarjeta3)
	if err != nil {
		log.Fatal(err)
	}

	//Comercio

	comercio1 := Comercio{1, "McDonals", "Av. Cabildo 2347", "C1428AAF", "011112234561"}
	data7, err := json.Marshal(comercio1)
	if err != nil {
		log.Fatal(err)
	}

	comercio2 := Comercio{2, "Burger King", "Av. Rivadavia 4364", "C1205AAQ", "011155605275"}
	data8, err := json.Marshal(comercio2)
	if err != nil {
		log.Fatal(err)

	}

	comercio3 := Comercio{3, "Mostaza", "Av. Pres. Juan Domingo Peron 1522", "B1663GHR", "011155746850"}
	data9, err := json.Marshal(comercio3)
	if err != nil {
		log.Fatal(err)
	}

	//Compras

	compra1 := Compras{
		NroOperacion: 1,
		NroTarjeta:   tarjeta2.NroTarjeta,
		NroComercio:  comercio1.NroComercio,
		Fecha:        "2021-06-05",
		Monto:        14.005,
		Pagado:       false,
	}
	data10, err := json.Marshal(compra1)
	if err != nil {
		log.Fatal(err)
	}

	compra2 := Compras{
		NroOperacion: 2,
		NroTarjeta:   tarjeta1.NroTarjeta,
		NroComercio:  comercio2.NroComercio,
		Fecha:        "2021-06-05",
		Monto:        12.00,
		Pagado:       false,
	}
	data11, err := json.Marshal(compra2)
	if err != nil {
		log.Fatal(err)
	}

	compra3 := Compras{
		NroOperacion: 3,
		NroTarjeta:   tarjeta1.NroTarjeta, // fixme
		NroComercio:  comercio3.NroComercio,
		Fecha:        "2021-06-05",
		Monto:        1.00,
		Pagado:       false,
	}
	data12, err := json.Marshal(compra3)
	if err != nil {
		log.Fatal(err)

	}


	CreateUpdate(db, "cliente", []byte(strconv.Itoa(cliente1.NroCliente)), data)

	CreateUpdate(db, "cliente", []byte(strconv.Itoa(cliente2.NroCliente)), data2)

	CreateUpdate(db, "cliente", []byte(strconv.Itoa(cliente3.NroCliente)), data3)
	
	
	CreateUpdate(db, "Tarjeta", []byte((tarjeta1.NroTarjeta)), data4)

	CreateUpdate(db, "Tarjeta", []byte((tarjeta2.NroTarjeta)), data5)

	CreateUpdate(db, "Tarjeta", []byte((tarjeta3.NroTarjeta)), data6)
	

	CreateUpdate(db, "comercio", []byte(strconv.Itoa(comercio1.NroComercio)), data7)

	CreateUpdate(db, "comercio", []byte(strconv.Itoa(comercio2.NroComercio)), data8)

	CreateUpdate(db, "comercio", []byte(strconv.Itoa(comercio2.NroComercio)), data9)


	CreateUpdate(db, "Compra", []byte(strconv.Itoa(compra1.NroOperacion)), data10)

	CreateUpdate(db, "Compra", []byte(strconv.Itoa(compra2.NroOperacion)), data11)

	CreateUpdate(db, "Compra", []byte(strconv.Itoa(compra3.NroOperacion)), data12)

}
