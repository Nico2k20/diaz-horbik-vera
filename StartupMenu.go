package main

import (
	"database/sql"
	"fmt"
	_"github.com/lib/pq"
	"log"
)

func createDatabase() {
    db,err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable")
    if err != nil {
        log.Fatal(err)
    }
    defer db.Close()

    _, err = db.Exec(`create database  tarjetadecredito;`)
    if err != nil {
        log.Fatal(err)
    }
}


func createtables() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjetadecredito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	 defer db.Close()
	
	_,err = db.Exec(`create table cliente(nrocliente int, nombre text, apellido text, domicilio text, telefono char(12));
					create table tarjeta( nrotarjeta char(16),nrocliente int, validadesde char(6), validahasta char(6), codseguridad	char(4),limitecompra	decimal(8,2),estado char(10));
			  		create table comercio(nrocomercio	int, nombre text, domicilio text, codigopostal char(8), telefono char(12));
			  		create table compra(nro_operacion	serial, nrotarjeta char(16), nrocomercio int, fecha	timestamp, monto decimal(7,2),pagado boolean );
			  		create table rechazo( nrorechazo	serial, nrotarjeta char(16), nrocomercio  int,fecha   timestamp, monto decimal(7,2), motivo text);
					create table cierre(año int, mes int,terminacion  int,fechainicio  date, fechacierre date, fechavto date );
		      	  	create table cabecera(nroresumen serial, nombre text, apellido text,domicilio text, nrotarjeta char(16),desde date, hasta date,vence date,total decimal(8,2));
			  		create table detalle(nroresumen int, nrolinea serial, fecha	date, nombrecomercio	text,monto decimal(7,2));
			  		create table alerta(nroalerta	serial,nrotarjeta	char(16),fecha	timestamp,nrorechazo int,codalerta int, descripcion	text );
			  		create table consumo(nrotarjeta		char(16),codseguridad	char(4),nrocomercio	int,monto decimal(7,2)); `)
    	if err != nil {
        log.Fatal(err)
    }
    
}

func crearPksYFks() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjetadecredito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	 defer db.Close()
	
	_,err = db.Exec(` 
   				alter table cliente 	add constraint cliente_pk 		primary key (nrocliente);
				alter table tarjeta 	add constraint tarjeta_pk 		primary key (nrotarjeta);
				alter table comercio 	add constraint comercio_pk 		primary key (nrocomercio);
				alter table compra 		add constraint compra_pk 		primary key (nro_operacion);
				alter table rechazo 	add constraint rechazo_pk 		primary key (nrorechazo);	
				alter table cierre 		add constraint cierre_pk 		primary key (año, mes, terminacion);
				alter table cabecera 	add constraint cabecera_pk 		primary key (nroresumen);
				alter table detalle 	add constraint detalle_pk 		primary key (nroresumen, nrolinea);
				alter table alerta 		add constraint alerta_pk 		primary key (nroalerta);

	

				alter table tarjeta		add constraint tarjeta_nrocliente_fk 		foreign key (nrocliente) references cliente(nrocliente);
				alter table compra 		add constraint compra_nrotarjeta_fk 		foreign key (nrotarjeta) references tarjeta(nrotarjeta);
				alter table compra 		add constraint compra_nrocomercio_fk 		foreign key (nrocomercio)references comercio(nrocomercio);
				alter table cabecera 	add constraint cabecera_nrotarjeta_fk 		foreign key (nrotarjeta) references tarjeta(nrotarjeta);
				alter table detalle 	add constraint detalle_nroresumen_fk 		foreign key (nroresumen) references cabecera(nroresumen);`)
	 
	 if err != nil {
        	log.Fatal(err)
    		}    


}



func eliminarPksYFks() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjetadecredito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	 defer db.Close()
	
	_,err = db.Exec(` 
   					ALTER TABLE tarjeta 	DROP CONSTRAINT tarjeta_nrocliente_fk;
   					ALTER TABLE compra 		DROP CONSTRAINT compra_nrotarjeta_fk;
    				ALTER TABLE compra 		DROP CONSTRAINT compra_nrocomercio_fk;
    				ALTER TABLE cabecera 	DROP CONSTRAINT cabecera_nrotarjeta_fk;
    				ALTER TABLE detalle 	DROP CONSTRAINT detalle_nroresumen_fk;
        
    				ALTER TABLE cliente 	DROP CONSTRAINT cliente_pk;
    				ALTER TABLE tarjeta 	DROP CONSTRAINT tarjeta_pk;
    				ALTER TABLE comercio 	DROP CONSTRAINT comercio_pk;
    				ALTER TABLE compra 		DROP CONSTRAINT compra_pk;
    				ALTER TABLE rechazo 	DROP CONSTRAINT rechazo_pk;
    				ALTER TABLE cierre 		DROP CONSTRAINT cierre_pk;
    				ALTER TABLE cabecera 	DROP CONSTRAINT cabecera_pk;
    				ALTER TABLE detalle 	DROP CONSTRAINT detalle_pk;
    				ALTER TABLE alerta 		DROP CONSTRAINT alerta_pk;`)
	 
	 if err != nil {
        	log.Fatal(err)
    		}    


}


func eliminarTablas() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjetadecredito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	 defer db.Close()
	
	_,err = db.Exec(` 
					drop table  cliente;
					drop table  tarjeta;
					drop table  comercio;
					drop table  cierre;
					drop table  consumo;
					drop table  detalle;
					drop table  cabecera;
					drop table  compra;
					drop table  rechazo;
					drop table  alerta;`)
	 if err != nil {
        	log.Fatal(err)
    }    
}

func llenartablas() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjetadecredito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	 defer db.Close()
	
	_, err = db.Exec(`
	
				insert into cliente values (1, 'Jorge',    'Catalanes','Callao 1415',             '011147040798');
				insert into cliente values (2, 'Catalina', 'Barrios',  'Juan Francisco Segui 845','011153467824');
				insert into cliente values (3, 'Samuel',   'Davile',   'Nuñez 2530',              '011589675423');
				insert into cliente values (4, 'Agustina', 'Palacios', 'Hipolito Yrigoyen 1569',  '011135751589');
				insert into cliente values (5, 'Alexis',   'Fuentes',  'Yatasto 8279',            '011567894536');
				insert into cliente values (6, 'Denise',   'Quiroz',   'Santos Vega 673',         '011189673492');
				insert into cliente values (7, 'Ricardo',  'Alanis',   'Maipu 103',               '011523845291');
				insert into cliente values (8, 'Marga',    'Santos',   'Horacio Quiroga 1205',    '011579939421');
				insert into cliente values (9, 'Felipe',   'Acuña',    'José Colombres 2478',     '011154795368');
				insert into cliente values (10,'Felicia',  'Serrano',  'Valparaiso 830',          '011170984573');
				insert into cliente values (11,'Matias',   'Smith',    'Santos Vega 1510',        '011149892900');
				insert into cliente values (12,'Ailen',    'Rosales',  'Muñoz 2900',              '011191002337');
				insert into cliente values (13,'Francis',  'Rodriguez','Florentino Ameghino 885', '011560080634');
				insert into cliente values (14,'Carolina', 'Grassi',   'Rio Negro 1175',          '011567733421');
				insert into cliente values (15,'Adrian',   'Gomez',    'Tomas Gody Cruz 449',     '011110348614');
				insert into cliente values (16,'Laura',    'Calio',    'Uruguay 2570',            '011578992365');
				insert into cliente values (17,'José',     'Robles',   'Esteban Gomez 3493',      '011553890122');
				insert into cliente values (18,'Fatima',   'Carrazco', 'Hiroshima 1115',          '011134789256');
				insert into cliente values (19,'Luis',     'Zamorano', 'Francisco Beiró 4565',    '011167895628');
				insert into cliente values (20,'Elizabeth','Nielsen',  'Formosa 1551',            '011567951232');

				insert into tarjeta values ('4539608248204879', 1,  '201907', '202407', '603', 1000.00,    'vigente');
				insert into tarjeta values ('4520139769562264', 2,  '201811', '202311', '623', 1222.00,    'anulada');
				insert into tarjeta values ('4017815492979764', 3,  '201609', '202109', '506', 6000.00,    'vigente');
				insert into tarjeta values ('4325778640635281', 4,  '201711', '202211', '805', 9000.00,    'suspendida');
				insert into tarjeta values ('4658368747812582', 5,  '201807', '202307', '755', 7000.52,    'suspendida');
				insert into tarjeta values ('4969297201303610', 6,  '201802', '202302', '999', 2000.25,    'vigente');
				insert into tarjeta values ('4784572716003993', 7,  '201801', '202301', '970', 6600.55,    'vigente');
				insert into tarjeta values ('4406473989433629', 8,  '201803', '202303', '387', 9000.65,    'vigente');
				insert into tarjeta values ('4597208357621796', 9,  '202101', '202601', '788', 8000.88,    'anulada');
				insert into tarjeta values ('4381351563386232', 10, '201705', '202205', '779', 1000.99,    'vigente');
				insert into tarjeta values ('4276317192464730', 11, '201712', '202212', '454', 7000.10,    'vigente');
				insert into tarjeta values ('4117153889927504', 12, '201610', '202110', '202', 4000.95,    'anulada');
				insert into tarjeta values ('4484922874607924', 13, '201912', '202412', '125', 8800.50,    'vigente');
				insert into tarjeta values ('343351557262279',  14, '201711', '202211', '502', 1400.50,    'suspendida');
				insert into tarjeta values ('345702611565180',  15, '202002', '202502', '378', 7000.00,    'vigente');
				insert into tarjeta values ('342319819396145',  16, '202104', '202604', '731', 5000.00,    'vigente');
				insert into tarjeta values ('340182710948311',  17, '201906', '202406', '755', 4300.00,    'vigente');
				insert into tarjeta values ('378506903333441',  18, '201911', '202411', '463', 9000.46,    'anulada');
				insert into tarjeta values ('377336717013125',  19, '201903', '202403', '808', 2000.48,    'vigente');
				insert into tarjeta values ('347566910320151',  20, '202008', '202508', '141', 10000.56,   'vigente');
				insert into tarjeta values ('374824927430539',  10, '202012', '202102', '417', 20000.55,   'vigente');
				insert into tarjeta values ('4157906281101924', 20, '201705', '202005', '270', 5000.00,     'vigente');

				insert into comercio values (1,  'McDonals',   'Av. Cabildo 2347',                 'C1428AAF', '011112234561');
				insert into comercio values (2,  'Burger King','Av. Rivadavia 4364',               'C1428AAF', '011155605275');
				insert into comercio values (3,  'Mostaza',    'Av. Pres. Juan Domingo Peron 1522','B1663GHR', '011155746850');
				insert into comercio values (4,  'KFC',        'Av. Corrientes 4222',              'C1195AAO', '011112651920');
				insert into comercio values (5,  'Starbucks',  'Juramento 2558',                   'C1428DNR', '011115360085');
				insert into comercio values (6,  'Subway',     'Av. Cordoba 1315',                 'C1055AAD', '011155674350');
				insert into comercio values (7,  'Wendys',     'Carlos Pellegrini 899',            'C1009ABQ', '011153551061');
				insert into comercio values (8,  'Freddo',     'Av. Callao',                       'C1023AAS', '011126529138');
				insert into comercio values (9,  'OpenSports', 'Av. Presidente Peron 1622',        'B1663GHS', '011119149994');
				insert into comercio values (10, 'Farmacity',  'El Callao 955',                    'B1615DCT', '011112642854');
				insert into comercio values (11, 'Grimoldi',   'El Plumerillo 1498',               'B1615DGB', '011150218106');
				insert into comercio values (12, 'Fravega',    'Av. Santa Fe 1650',                'C1060ABP', '011156910640');
				insert into comercio values (13, 'Garbarino',  'Paraguay 4979',                    'C1425BTC', '011151764955');
				insert into comercio values (14, 'Musimundo',  'Paunero 1479',                     'B1663GJG', '011890572433');
				insert into comercio values (15, 'Yenny',      'Paraná 3745',                      'B1640FRE', '011154821230');
				insert into comercio values (16, 'Diadora',    'Blanco Encalada 42',               'B1609EEN', '011155611614');
				insert into comercio values (17, 'Puma',       'Av. Crámer 3664',                  'C1429AJP', '011171213115');
				insert into comercio values (18, 'Nike',       'Las Magnolias 754',                'B1629PGF', '011153558893');
				insert into comercio values (19, 'Adidas',     'Av. Corrientes 3247',              'C1193AAE', '011151820388');
				insert into comercio values (20, 'Sarkany',    'Av. Santa Fe 3253',                'C1425BGH', '011159235741');
		`)
   	
   	if err != nil {
		log.Fatal(err)
	}

}

func insertarConsumos(){
db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjetadecredito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	 defer db.Close()
	
	_, err = db.Exec(`
				
				/*Tarjetas inexistentes*/
				insert into consumo values('5017815492979764','506',2,500.00);
				insert into consumo values('5117815492979764','506',2,500.00);
				insert into consumo values('6017815492979764','506',2,500.00);
				insert into consumo values('9017815492979764','506',2,500.00);
				
				
				/*si validas*/
				insert into consumo values('4406473989433629','387',2,1500.00);
				insert into consumo values('4406473989433629','387',2,2000.00);
				insert into consumo values('4406473989433629','387',2,500.00);
				insert into consumo values('4406473989433629','387',2,1000.00);
				insert into consumo values('4406473989433629','387',2,1800.00);
				insert into consumo values('4406473989433629','387',2,1000.00);
				insert into consumo values('4406473989433629','387',2,1000.00);
				
				/*NO VALIDAS*/
				insert into consumo values('4406473989433629','387',2,1000.00);
				insert into consumo values('4406473989433629','387',2,1000.00);
				insert into consumo values('4406473989433629','387',2,1000.00);
				insert into consumo values('4406473989433629','387',2,1000.00);
				
				
				/*Tarjeta no vigente*/
				
				insert into consumo values('378506903333441','463',2,500.00);
				insert into consumo values('4597208357621796','788',2,500.00);
				insert into consumo values('378506903333441','463',2,400.00);
				insert into consumo values (343351557262279,'502',2,300.00);
				
				/*Tarjeta vencida*/
				
				insert into consumo values(4157906281101924,'270',2, 465.00);
				insert into consumo values('374824927430539','417',2,10.00);
				
				/*Tarjetas con cod seguridad no valido*/
				
				insert into consumo values('4539608248204879','621',1,992.0);
				insert into consumo values('4539608248204879','614',2,10);
				
				/*tarjeta anulada*/
				
				insert into consumo values('4117153889927504','202',10,55);
			
				/*no aceptada*/
				
				insert into consumo values('4276317192464730','454',2,23000.00);
				
				/*si aceptada*/
				
				insert into consumo values('4276317192464730','454',2,10.00);
	
				/*si acepta*/
				
				insert into consumo values ('4969297201303610', '999',2,1000.00);

				/*no acepta*/
				
				insert into consumo values ('4969297201303610', '999',2,30000.00);

				/*si acepta*/
				
				insert into consumo values ('4969297201303610', '999',2,10.00);
	
				/*No acepta */
				
				insert into consumo values ('4969297201303610', '999',2,90000.00);
				
				/*suspendida*/
				
				insert into consumo values ('4969297201303610', '999',2,100.00);			
				
				/* prueba */ 
				
				insert into consumo values('4017815492979764','506',2,500.00);
				insert into consumo values('4539608248204879','603',1,992.0);
				insert into consumo values('4539608248204879','603',2,10);
				insert into consumo values('4520139769562264','623',2,40.00);
				insert into consumo values('4017815492979764','506',1,20.00);
				insert into consumo values('4017815492979764','506',3,20.00);
				insert into consumo values('4325778640635281','805',2,40.00);
			
			
			
				
			/* PROBANDO RESUMEN CLIENTE 2 TARJETAS*/
				
			insert into compra (
									nroTarjeta,		  
		 							nroComercio,			   
									fecha,		
									monto,			
									pagado)
									values ('347566910320151',11,'2021-05-17 21:40:00',200.00,false);
				insert into compra (
									nroTarjeta,		  
		 							nroComercio,			   
									fecha,		
									monto,			
									pagado)
									values ('347566910320151',15,'2021-05-13 21:00:00',90.00,false); 
				insert into compra (
									nroTarjeta,		  
		 							nroComercio,			   
									fecha,		
									monto,			
									pagado)
									values ('347566910320151',19,'2021-05-05 19:49:00',780.00,false); 
									
				insert into compra (
									nroTarjeta,		  
		 							nroComercio,			   
									fecha,		
									monto,			
									pagado)
									values ('4157906281101924',13,'2021-05-20 10:49:00',90.00,false);
				insert into compra (
									nroTarjeta,		  
		 							nroComercio,			   
									fecha,		
									monto,			
									pagado)
									values ('4157906281101924',10,'2021-05-13 11:49:00',500.00,false);
				insert into compra (
									nroTarjeta,		  
		 							nroComercio,			   
									fecha,		
									monto,			
									pagado)
									values ('4157906281101924',9,'2021-05-27 22:49:00',88.00,false); 
			
			
			
				/*COMPRAS QUE EN LAS QUE DEBE SALTAR LA ALERTA DE 5 */

				insert into compra (
									nroTarjeta,		  
		 							nroComercio,			   
									fecha,		
									monto,			
									pagado)
									values ('345702611565180',3,'2021-06-13 21:49:00',9.00,false);	

				insert into compra (
									nroTarjeta,		  
		 							nroComercio,			   
									fecha,		
									monto,			
									pagado)
									values ('345702611565180',4,'2021-06-13 21:51:00',15.00,false);	

				/*Compra que NOOOOO debe saltar en alerta 5 minutos*/
				insert into compra (
									nroTarjeta,		  
		 							nroComercio,			   
									fecha,		
									monto,			
									pagado)
									values ('345702611565180',5,'2021-06-13 22:00:00',12.00,false);	

				/*Alertas compras 1 minuto*/
				insert into compra (
									nroTarjeta,		  
		 							nroComercio,			   
									fecha,		
									monto,							
									pagado)
									values ('345702611565180',1,'2021-06-13 21:45:00',85.00,false);	

				insert into compra (
									nroTarjeta,		  
		 							nroComercio,			   
									fecha,		
									monto,							
									pagado)
									values ('345702611565180',2,'2021-06-13 21:46:00',85.00,false);	

				/*Compra que no deberia saltar la alerta 1 min*/
				insert into compra (
									nroTarjeta,		  
		 							nroComercio,			   
									fecha,		
									monto,							
									pagado)
									values ('345702611565180',2,'2021-06-13 21:49:00',85.00,false);`)
   	if err != nil {
		log.Fatal(err)
	}

}

func insertarCierres(){
db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjetadecredito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	 defer db.Close()
	
	_, err = db.Exec(`	
		insert into cierre values (2021,1, 0,'2021-1-1','2021-1-11','2021-01-31');
		insert into cierre values (2021,1, 1,'2021-1-2','2021-1-12','2021-01-31');
		insert into cierre values (2021,1, 2,'2021-1-4','2021-1-14','2021-01-31');
		insert into cierre values (2021,1, 3,'2021-1-6','2021-1-16','2021-01-31');
		insert into cierre values (2021,1, 4,'2021-1-8','2021-1-18','2021-01-31');
		insert into cierre values (2021,1, 5,'2021-1-10','2021-1-20','2021-01-31');
		insert into cierre values (2021,1, 6,'2021-1-12','2021-1-22','2021-01-31');
		insert into cierre values (2021,1, 7,'2021-1-14','2021-1-24','2021-01-31');
		insert into cierre values (2021,1, 8,'2021-1-16','2021-1-26','2021-01-31');
		insert into cierre values (2021,1, 9,'2021-1-18','2021-1-28','2021-01-31');
		insert into cierre values (2021,2, 0,'2021-2-1','2021-2-11','2021-02-28');
		insert into cierre values (2021,2, 1,'2021-2-2','2021-2-12','2021-02-28');
		insert into cierre values (2021,2, 2,'2021-2-4','2021-2-14','2021-02-28');
		insert into cierre values (2021,2, 3,'2021-2-6','2021-2-16','2021-02-28');
		insert into cierre values (2021,2, 4,'2021-2-8','2021-2-18','2021-02-28');
		insert into cierre values (2021,2, 5,'2021-2-10','2021-2-20','2021-02-28');
		insert into cierre values (2021,2, 6,'2021-2-12','2021-2-22','2021-02-28');
		insert into cierre values (2021,2, 7,'2021-2-14','2021-2-24','2021-02-28');
		insert into cierre values (2021,2, 8,'2021-2-16','2021-2-26','2021-02-28');
		insert into cierre values (2021,2, 9,'2021-2-18','2021-2-28','2021-02-28');
		insert into cierre values (2021,3, 0,'2021-3-1','2021-3-11','2021-03-31');
		insert into cierre values (2021,3, 1,'2021-3-2','2021-3-12','2021-03-31');
		insert into cierre values (2021,3, 2,'2021-3-4','2021-3-14','2021-03-31');
		insert into cierre values (2021,3, 3,'2021-3-6','2021-3-16','2021-03-31');
		insert into cierre values (2021,3, 4,'2021-3-8','2021-3-18','2021-03-31');
		insert into cierre values (2021,3, 5,'2021-3-10','2021-3-20','2021-03-31');
		insert into cierre values (2021,3, 6,'2021-3-12','2021-3-22','2021-03-31');
		insert into cierre values (2021,3, 7,'2021-3-14','2021-3-24','2021-03-31');
		insert into cierre values (2021,3, 8,'2021-3-16','2021-3-26','2021-03-31');
		insert into cierre values (2021,3, 9,'2021-3-18','2021-3-28','2021-03-31');
		insert into cierre values (2021,4, 0,'2021-4-1','2021-4-11','2021-04-30');
		insert into cierre values (2021,4, 1,'2021-4-2','2021-4-12','2021-04-30');
		insert into cierre values (2021,4, 2,'2021-4-4','2021-4-14','2021-04-30');
		insert into cierre values (2021,4, 3,'2021-4-6','2021-4-16','2021-04-30');
		insert into cierre values (2021,4, 4,'2021-4-8','2021-4-18','2021-04-30');
		insert into cierre values (2021,4, 5,'2021-4-10','2021-4-20','2021-04-30');
		insert into cierre values (2021,4, 6,'2021-4-12','2021-4-22','2021-04-30');
		insert into cierre values (2021,4, 7,'2021-4-14','2021-4-24','2021-04-30');
		insert into cierre values (2021,4, 8,'2021-4-16','2021-4-26','2021-04-30');
		insert into cierre values (2021,4, 9,'2021-4-18','2021-4-28','2021-04-30');
		insert into cierre values (2021,5, 0,'2021-5-1','2021-5-11','2021-05-31');
		insert into cierre values (2021,5, 1,'2021-5-2','2021-5-12','2021-05-31');
		insert into cierre values (2021,5, 2,'2021-5-4','2021-5-14','2021-05-31');
		insert into cierre values (2021,5, 3,'2021-5-6','2021-5-16','2021-05-31');
		insert into cierre values (2021,5, 4,'2021-5-8','2021-5-18','2021-05-31');
		insert into cierre values (2021,5, 5,'2021-5-10','2021-5-20','2021-05-31');
		insert into cierre values (2021,5, 6,'2021-5-12','2021-5-22','2021-05-31');
		insert into cierre values (2021,5, 7,'2021-5-14','2021-5-24','2021-05-31');
		insert into cierre values (2021,5, 8,'2021-5-16','2021-5-26','2021-05-31');
		insert into cierre values (2021,5, 9,'2021-5-18','2021-5-28','2021-05-31');
		insert into cierre values (2021,6, 0,'2021-6-1','2021-6-11','2021-06-30');
		insert into cierre values (2021,6, 1,'2021-6-2','2021-6-12','2021-06-30');
		insert into cierre values (2021,6, 2,'2021-6-4','2021-6-14','2021-06-30');
		insert into cierre values (2021,6, 3,'2021-6-6','2021-6-16','2021-06-30');
		insert into cierre values (2021,6, 4,'2021-6-8','2021-6-18','2021-06-30');
		insert into cierre values (2021,6, 5,'2021-6-10','2021-6-20','2021-06-30');
		insert into cierre values (2021,6, 6,'2021-6-12','2021-6-22','2021-06-30');
		insert into cierre values (2021,6, 7,'2021-6-14','2021-6-24','2021-06-30');
		insert into cierre values (2021,6, 8,'2021-6-16','2021-6-26','2021-06-30');
		insert into cierre values (2021,6, 9,'2021-6-18','2021-6-28','2021-06-30');
		insert into cierre values (2021,7, 0,'2021-7-1','2021-7-11','2021-07-31');
		insert into cierre values (2021,7, 1,'2021-7-2','2021-7-12','2021-07-31');
		insert into cierre values (2021,7, 2,'2021-7-4','2021-7-14','2021-07-31');
		insert into cierre values (2021,7, 3,'2021-7-6','2021-7-16','2021-07-31');
		insert into cierre values (2021,7, 4,'2021-7-8','2021-7-18','2021-07-31');
		insert into cierre values (2021,7, 5,'2021-7-10','2021-7-20','2021-07-31');
		insert into cierre values (2021,7, 6,'2021-7-12','2021-7-22','2021-07-31');
		insert into cierre values (2021,7, 7,'2021-7-14','2021-7-24','2021-07-31');
		insert into cierre values (2021,7, 8,'2021-7-16','2021-7-26','2021-07-31');
		insert into cierre values (2021,7, 9,'2021-7-18','2021-7-28','2021-07-31');
		insert into cierre values (2021,8, 0,'2021-8-1','2021-8-11','2021-08-31');
		insert into cierre values (2021,8, 1,'2021-8-2','2021-8-12','2021-08-31');
		insert into cierre values (2021,8, 2,'2021-8-4','2021-8-14','2021-08-31');
		insert into cierre values (2021,8, 3,'2021-8-6','2021-8-16','2021-08-31');
		insert into cierre values (2021,8, 4,'2021-8-8','2021-8-18','2021-08-31');
		insert into cierre values (2021,8, 5,'2021-8-10','2021-8-20','2021-08-31');
		insert into cierre values (2021,8, 6,'2021-8-12','2021-8-22','2021-08-31');
		insert into cierre values (2021,8, 7,'2021-8-14','2021-8-24','2021-08-31');
		insert into cierre values (2021,8, 8,'2021-8-16','2021-8-26','2021-08-31');
		insert into cierre values (2021,8, 9,'2021-8-18','2021-8-28','2021-08-31');
		insert into cierre values (2021,9, 0,'2021-9-1','2021-9-11','2021-09-30');
		insert into cierre values (2021,9, 1,'2021-9-2','2021-9-12','2021-09-30');
		insert into cierre values (2021,9, 2,'2021-9-4','2021-9-14','2021-09-30');	
		insert into cierre values (2021,9, 3,'2021-9-6','2021-9-16','2021-09-30');
		insert into cierre values (2021,9, 4,'2021-9-8','2021-9-18','2021-09-30');
		insert into cierre values (2021,9, 5,'2021-9-10','2021-9-20','2021-09-30');
		insert into cierre values (2021,9, 6,'2021-9-12','2021-9-22','2021-09-30');
		insert into cierre values (2021,9, 7,'2021-9-14','2021-9-24','2021-09-30');
		insert into cierre values (2021,9, 8,'2021-9-16','2021-9-26','2021-09-30');
		insert into cierre values (2021,9, 9,'2021-9-18','2021-9-28','2021-09-30');
		insert into cierre values (2021,10,0,'2021-10-1','2021-10-11','2021-10-31');
		insert into cierre values (2021,10,1,'2021-10-2','2021-10-12','2021-10-31');
		insert into cierre values (2021,10,2,'2021-10-4','2021-10-14','2021-10-31');
		insert into cierre values (2021,10,3,'2021-10-6','2021-10-16','2021-10-31');
		insert into cierre values (2021,10,4,'2021-10-8','2021-10-18','2021-10-31');
		insert into cierre values (2021,10,5,'2021-10-10','2021-10-20','2021-10-31');
		insert into cierre values (2021,10,6,'2021-10-12','2021-10-22','2021-10-31');
		insert into cierre values (2021,10,7,'2021-10-14','2021-10-24','2021-10-31');
		insert into cierre values (2021,10,8,'2021-10-16','2021-10-26','2021-10-31');
		insert into cierre values (2021,10,9,'2021-10-18','2021-10-28','2021-10-31');
		insert into cierre values (2021,11,0,'2021-11-1','2021-11-11','2021-11-30');
		insert into cierre values (2021,11,1,'2021-11-2','2021-11-12','2021-11-30');
		insert into cierre values (2021,11,2,'2021-11-4','2021-11-14','2021-11-30');
		insert into cierre values (2021,11,3,'2021-11-6','2021-11-16','2021-11-30');
		insert into cierre values (2021,11,4,'2021-11-8','2021-11-18','2021-11-30');
		insert into cierre values (2021,11,5,'2021-11-10','2021-11-20','2021-11-30');
		insert into cierre values (2021,11,6,'2021-11-12','2021-11-22','2021-11-30');
		insert into cierre values (2021,11,7,'2021-11-14','2021-11-24','2021-11-30');
		insert into cierre values (2021,11,8,'2021-11-16','2021-11-26','2021-11-30');
		insert into cierre values (2021,11,9,'2021-11-18','2021-11-28','2021-11-30');
		insert into cierre values (2021,12,0,'2021-12-1','2021-12-11','2021-12-31');
		insert into cierre values (2021,12,1,'2021-12-2','2021-12-12','2021-12-31');
		insert into cierre values (2021,12,2,'2021-12-4','2021-12-14','2021-12-31');
		insert into cierre values (2021,12,3,'2021-12-6','2021-12-16','2021-12-31');
		insert into cierre values (2021,12,4,'2021-12-8','2021-12-18','2021-12-31');
		insert into cierre values (2021,12,5,'2021-12-10','2021-12-20','2021-12-31');
		insert into cierre values (2021,12,6,'2021-12-12','2021-12-22','2021-12-31');
		insert into cierre values (2021,12,7,'2021-12-14','2021-12-24','2021-12-31');
		insert into cierre values (2021,12,8,'2021-12-16','2021-12-26','2021-12-31');
		insert into cierre values (2021,12,9,'2021-12-18','2021-12-28','2021-12-31');`)
   	
   	if err != nil {
		log.Fatal(err)
	}

}

func crearStoredProcedure(){
db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjetadecredito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
	 defer db.Close()
	
	_, err = db.Exec(`create or replace function existetarjeta(numtarjeta char(16)) returns boolean as $$
					declare
					fila record;
					autorizado boolean;
					begin
						select * into fila from tarjeta where (nrotarjeta=numtarjeta and estado!='anulada');
		
						if not found then
							autorizado :=false;
							return autorizado;
		
						end if; 

						autorizado := true;
		
						return autorizado;

					end;
					$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(`create or replace function verificarcodigotarjeta(codigoseguridad char(4),numtarjeta char(16)) returns boolean as $$
				declare
				seguridad char (4);
				autorizado boolean;
				begin
					select codseguridad into seguridad from tarjeta where (nrotarjeta=numtarjeta and codseguridad=codigoseguridad);
		
					if not found then
						autorizado :=false;
						return autorizado;
		
					end if; 

					autorizado := true;
		
					return autorizado;

				end;
				$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(`create function mayor_a_limite(tarjeta_num char(16), limite decimal(8,2), filaactual record) returns boolean as $$


			declare
			sumatoria_compra record;
			buscar_tarjeta record;
			sumatoria decimal (8,2);
			autorizado boolean;
			begin
				sumatoria := (select sum(monto) from consumo
					where(nrotarjeta=tarjeta_num and codseguridad=filaactual.codseguridad and monto<=limite and filaactual.monto<=limite));
		
				select * into buscar_tarjeta from tarjeta where (nrotarjeta=tarjeta_num);
	
				if(buscar_tarjeta.estado='suspendida') then
					return true;		
				end if;	
	
				if sumatoria=0.00 then 
	
					autorizado=false;
					return autorizado;
					
				end if;
	
				if sumatoria<=limite  then
					autorizado := true;
					return autorizado;
	
				else 
					autorizado := false;
					return autorizado;
	
				end if;

			end;

			$$ language plpgsql;`)

	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(`create or replace function verificarmonto(numtarjeta char(16), montoapagar decimal(7,2), fila_valor record) returns boolean as $$
					declare
					limite decimal (8,2);
					comparacion_limite_es_menor boolean;
					autorizado boolean;
					
					begin
						select limitecompra into limite from tarjeta where (nrotarjeta=numtarjeta);
						comparacion_limite_es_menor := mayor_a_limite(numtarjeta,limite,fila_valor);
						if comparacion_limite_es_menor then
							raise notice 'suma entro  limite ';
							autorizado := true;
							return autorizado;
						else 
							raise notice 'suma entro a false';
							autorizado := false;
							return autorizado;
						end if;	
					end;
					$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(`create or replace function verificarvencimiento(numtarjeta char(16)) returns boolean as $$
					declare
					estadotarjeta record;
					anovto char(4);
					mesvto char(2);
					mesactual int;
					anoactual   int;
					autorizado boolean;
					begin
	 
						select * into estadotarjeta from tarjeta where (nrotarjeta=numtarjeta and estado != 'anulada');
			
						if not found then
							autorizado :=false;
							return autorizado;
		
						end if; 
		
						anovto := (select estadotarjeta.validahasta)::CHAR(4);
		
						anoactual := Extract(YEAR FROM CURRENT_TIMESTAMP);
		
						if (anovto::int)>=anoactual then
			
			
							if (anovto::int)=anoactual then
			
								mesvto := (select substring(estadoTarjeta.validahasta from 5 for 6))::char(2);
			
								mesactual := Extract(MONTH FROM CURRENT_TIMESTAMP);
			
								if mesactual<=mesvto::int then
			
									autorizado := true;
									return autorizado;
								else
									autorizado :=false;
									return autorizado;	
					
								end if;
							end if;
			
							autorizado := true;
							return autorizado;
			
						end if ;
		
						autorizado := false;
		
						return autorizado;
			
					end;
					$$ language plpgsql;`)

	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(`create or replace function verificarsuspedida(numtarjeta char(16)) returns boolean as $$
					declare
					estadotarjeta char(10);
					autorizado boolean;
					begin
						select estado into estadotarjeta from tarjeta where (nrotarjeta=numtarjeta and estado = 'vigente');
						if not found then
							autorizado :=false;
							return autorizado;
						end if; 
						autorizado := true;
						return autorizado;

					end;
					$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(`create or replace function autorizacioncompra(v record) returns boolean as $$
					declare
					nroderechazo int;
					latarjetaexiste boolean;
					codseguridadverificado boolean;
					limiteenrango boolean;
					novencida boolean;
					nosuspendida boolean;
					begin
						latarjetaexiste := existetarjeta(v.nrotarjeta);
						if latarjetaexiste then
							raise notice 'existe la tarjeta';
			
							codseguridadverificado:=verificarcodigotarjeta(v.codseguridad,v.nrotarjeta);
			
							if codseguridadverificado then
								raise notice 'cod de seguridad equivalente';
			
								limiteenrango := verificarmonto(v.nrotarjeta,v.monto,v);
				
								if limiteenrango then
									raise notice 'monto a pagar aceptado';
				
									novencida=verificarvencimiento(v.nrotarjeta);
					
									if novencida then
										raise notice 'La tarjeta es valida';
						
										nosuspendida := verificarsuspedida(v.nrotarjeta);					
											
										if nosuspendida then
											raise notice 'Compra aceptada';
							
											insert into compra (
															nrotarjeta,		  
															nrocomercio,			   
															fecha,		
															monto,			
															pagado)
															values (v.nrotarjeta,v.nrocomercio,CURRENT_TIMESTAMP,v.monto,false);					
										else
											insert into rechazo(
															nrotarjeta,  
															nrocomercio,	   
															fecha,
															monto,
															motivo)
															values(v.nrotarjeta,v.nrocomercio,CURRENT_TIMESTAMP,v.monto,'la tarjeta se encuentra suspendida');						
											return false;
										end if;			
									else
										insert into rechazo(
														nrotarjeta,
														nrocomercio,
														fecha,
														monto,
														motivo)
														values(v.nrotarjeta,v.nrocomercio,CURRENT_TIMESTAMP,v.monto,'plazo de vigencia expirado.');
										return false;	     	
									end if;
								else
									insert into rechazo (nrotarjeta,  
														nrocomercio,
														fecha,
														monto,
														motivo)
														values(v.nrotarjeta,v.nrocomercio,CURRENT_TIMESTAMP,v.monto,'supera limite tarjeta');
									return false;
								end if;
							else
								insert into rechazo	(nrotarjeta, 
													nrocomercio,	 
													fecha,
													monto, 
													motivo)
													values(v.nrotarjeta,v.nrocomercio,CURRENT_TIMESTAMP,v.monto,'Codigo de seguridad invalido');
								return false;
							end if;
						else
							insert into rechazo (nrotarjeta,
												nrocomercio,	  
												fecha,
												monto, 
												motivo)
												values(v.nrotarjeta,v.nrocomercio,CURRENT_TIMESTAMP,v.monto,'tarjeta no valida o no vigente');
							return false;
							end if;
						return true;
					end;
					$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(`create or replace function autorizacion() returns trigger as $$
					declare
					v record;
					resultado boolean;
					begin
						resultado:= autorizacioncompra(new);
						return new;
					end;
					$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(`create or replace function generarresumen(nroclientebuscado int, mesbuscado int) returns void as $$
					declare
					v record; 				-- para comparar cliente
					t record; 				-- para comparar tarjeta
					d record; 				-- para comparar cierres
					y record; 				-- para buscar el total
					l record; 				-- para comparar compra
					n record; 				-- para comercio
					desde date;
					hasta date;
					vence date;
					mesc int;     			-- para comparar los meses
					total decimal(8,2); 	-- para guardar la suma de todas las comprar
					nrores  int;  			-- para guardar el nro de resumen
					sepuede boolean;		-- para verificar que se pueda llenar cabecera
					nrol int;       		-- para ir contando los nro lineas
					begin
						sepuede := false;
						-- revisa que sea el mismo cliente
						select * into v from cliente where nrocliente = nroclientebuscado;
						if not found then
							raise notice 'El cliente % no existe',nroclientebuscado;
						else 
							raise notice 'El cliente % existe', nroclientebuscado;
						end if;
						-- revisa que sea la tarjeta del cliente buscado
						for t in select * from tarjeta where nrocliente = nroclientebuscado loop
							total := 0.0;       -- inicializo el total para que no este en null
							if not found then
								raise notice'Tarjeta no valida';
							end if;
							-- revisa que el cierre sea del mes ingresado, es para sacar el periodo
							select * into d from cierre where mes = mesbuscado;
							if not found then
								raise notice 'no existe el mes %', mesbuscado;
							else
								desde:= d.fechainicio;
								hasta:= d.fechacierre;
								vence:= d.fechavto;	
							end if;
							-- suma todos los montos de las compras del mes y da el total
							for y in select * from compra where (nrotarjeta = t.nrotarjeta)  loop	
								-- paso el mes de la compra a int 
								mesc:= EXTRACT(MONTH from y.fecha);
								if mesc  = mesbuscado then
									total := total + y.monto;
								else 
									raise notice' No hay compras en este mes';
								end if;	
								sepuede := true;
							end loop;  
							-- verifica que se pueda llenar la cabecera y ademas que el mes ingresado sea valido
							if sepuede = true and mesbuscado<= 12 then	
								-- empieza a llenar la tabla de cabecera
								insert into cabecera(nombre, apellido, domicilio, nrotarjeta, desde, hasta, vence, total) values (v.nombre, v.apellido, v.domicilio, t.nrotarjeta, desde, hasta, vence, total);
								raise notice 'Se guardo una cabecera';
							end if;
							--empieza a guardar detalle
							-- busco el nro de resumen de cabecera
							nrores:= (select COUNT(nroresumen) from cabecera);
							--corrobora que la fila de compra sea del mismo cliente y llena cada detalle por compra
							nrol := 0;
							for l in select * from compra where (nrotarjeta = t.nrotarjeta and t.nrocliente = nroclientebuscado and mesc = mesbuscado) loop
								nrol := nrol + 1;
								--llena los datos de detalle con el nro de resumen guardado en la variable nrores
								--para el nombre del comercio 
								select * into n from comercio where nrocomercio = l.nrocomercio;
								insert into detalle(nrolinea, nroresumen, fecha, nombrecomercio, monto) values (nrol, nrores, l.fecha, n.nombre, l.monto);
								update compra set pagado=true where mesc= EXTRACT(MONTH from fecha) and nrotarjeta = t.nrotarjeta;
								raise notice 'se guardo un detalle';
							end loop;
						end loop;
					end;
					$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(`create or replace function ingresaralertarechazo() returns trigger as $$
					begin
						insert into alerta (nrotarjeta,fecha,nrorechazo,codalerta,descripcion) values(new.nrotarjeta,CURRENT_TIMESTAMP,new.nrorechazo,0, 'Consumo rechazado');
						return new;
					end;
					$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`create or replace function ingresarAlertaCompra1min() returns void as $$
					declare
					v record;
					c record;
					intervalo interval;
					esMenor boolean;
					fila_invalida boolean;
					begin

						for v in select * from compra c1 ,comercio c2 where (c1.nroComercio=c2.nroComercio )  loop
		
							for c in select * from compra c3 ,comercio c4 where (c3.nroComercio=c4.nroComercio )   loop

								if (v.codigoPostal=c.codigoPostal) and (v.nro_operacion!=c.nro_operacion) and (v.nroTarjeta=c.nroTarjeta) then
								-- SI EL CODIGO POSTAL ES IGUAL Y EL NRO DE TARJETA ES IGUAL
								--Y EL NRO OPERACION ES DISTINTO(PARA EVITAR TOMAR LA MISMA FILA)
									if ((v.nroComercio = c.nroComercio)=false) then    -- y ademas los comercios son distintos 
										fila_invalida:=  validar_un_min(v,c);
										--se valida el intervalo entre compra si es true la compra es invalida
										if fila_invalida then 
											insert into alerta (nroTarjeta,fecha,nroRechazo,codalerta,descripcion) values(v.nroTarjeta,v.fecha,null,1,'Compra 1 minuto');				
										end if;
									end if;
								end if;
							end loop;
						end loop;
					end;
					$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}					
	_, err = db.Exec(`create or replace function validar_un_min(fila record,fila1 record) returns boolean as $$
					declare
					minuto int;
					hora int;
					dia int;	--MINUTOS HORAS DIAS.... DE LA FILA 1 
					mes  int;
					ano	int;

					minuto2 int;
					hora2 int;
					dia2 int;	--MINUTOS HORAS DIAS.... DE LA FILA 2 
					mes2  int;
					ano2	int;

					minuto_loop int;
					hora_loop int;
					dia_loop int;	--MINUTOS HORAS DIAS.... DE LAS FILAS A RECORRER EN EL LOOP 
					mes_loop int;
					ano_loop  int;

					encontre_alerta record;
					begin
						minuto := EXTRACT(MINUTE from fila.fecha);
						hora := EXTRACT(HOUR from fila.fecha);
						dia :=  EXTRACT(DAY from fila.fecha); 		-- EXTRAE LA INFO DE CADA MINUTO, HORA DIA ETC
						mes :=  EXTRACT(MONTH from fila.fecha);
						ano :=  EXTRACT(YEAR from fila.fecha);
					
						for encontre_alerta in select * from alerta where (nroTarjeta=fila.nroTarjeta) loop 
							minuto_loop :=   EXTRACT(MINUTE from encontre_alerta.fecha);
							hora_loop :=	 EXTRACT(HOUR from encontre_alerta.fecha);
							dia_loop :=		 EXTRACT(DAY from encontre_alerta.fecha);		
							-- SI YA EXISTE UNA ALERTA CON LOS MISMOS ATRIBUTOS SE DEVUELVE FALSE Y NO SE AGREGA UNA NUEVA ALERTA
							mes_loop :=		 EXTRACT(MONTH from encontre_alerta.fecha);
							ano_loop  :=	 EXTRACT(YEAR from encontre_alerta.fecha);

							if (minuto=minuto_loop and hora=hora_loop and dia = dia_loop and mes=mes_loop and ano=ano_loop and encontre_alerta.codAlerta=1) then
								return false;
							end if;
						end loop;
						minuto2 := EXTRACT(MINUTE from fila1.fecha);
						hora2 := EXTRACT(HOUR from fila1.fecha);
						dia2 :=  EXTRACT(DAY from fila1.fecha);	-- SI LAS ALERTAS COINCIDEN EN FECHA Y HORA SE HACE UNA DIFERENCIA CON LOS MINUTOS
						mes2 :=  EXTRACT(MONTH from fila1.fecha);
						ano2 :=  EXTRACT(YEAR from fila1.fecha);
						raise notice 'record1  % record', fila;	
						raise notice 'record2  % record', fila1;
						raise notice 'boolean % record', (fila.fecha-fila1.fecha) <='00:01:00'::interval and ((fila.fecha-fila1.fecha) >='00:00:00'::interval);
						if(mes=mes2 and dia=dia2 and hora=hora2) then
							if ((fila.fecha-fila1.fecha) <='00:01:00'::interval and (fila.fecha-fila1.fecha) >='00:00:00'::interval) then	-- SE COMPARA LA DIFERENCIA CON EL INTERVALO DESEADO Y CON 0 PARA EVITAR
								return true;
								--FECHAS REPETIDAS YA QUE DE LAS FECHAS NEGATIVAS SE TOMA SU MODULO EN
							end if; --EN LA COMPARACION 
						end if;
						return false;
					end;
					$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(`create or replace function validarintervalo5minutos(fila record,fila1 record) returns boolean as $$
					declare
					minuto int;
					hora int;
					dia int;                   --MINUTOS HORAS DIAS.... DE LA FILA 1 
					mes  int;
					ano	int;
					
					minuto2 int;
					hora2 int;
					dia2 int;			--MINUTOS HORAS DIAS.... DE LA FILA 2 
					mes2  int;
					ano2	int;
					
					minuto_loop int;
					hora_loop int;
					dia_loop int;			--MINUTOS HORAS DIAS.... DE LAS FILAS A RECORRER EN EL LOOP 
					mes_loop int;
					ano_loop  int;

					encontre_alerta record;
					begin
						minuto := EXTRACT(MINUTE from fila.fecha);
						hora := EXTRACT(HOUR from fila.fecha);
						dia :=  EXTRACT(DAY from fila.fecha);
						mes :=  EXTRACT(MONTH from fila.fecha);		-- EXTRAE LA INFO DE CADA MINUTO, HORA DIA ETC
						ano :=  EXTRACT(YEAR from fila.fecha);

						for encontre_alerta in select * from alerta where (nroTarjeta=fila.nroTarjeta) loop 
							minuto_loop :=   EXTRACT(MINUTE from encontre_alerta.fecha);
							hora_loop :=	 EXTRACT(HOUR from encontre_alerta.fecha);
							dia_loop :=		 EXTRACT(DAY from encontre_alerta.fecha);
							mes_loop :=		 EXTRACT(MONTH from encontre_alerta.fecha);	
							-- SI YA EXISTE UNA ALERTA CON LOS MISMOS ATRIBUTOS SE DEVUELVE FALSE Y NO SE AGREGA UNA NUEVA ALERTA
							ano_loop  :=	 EXTRACT(YEAR from encontre_alerta.fecha);
							if (minuto=minuto_loop and hora=hora_loop and dia = dia_loop and mes=mes_loop and ano=ano_loop and encontre_alerta.codAlerta=5) then
								return false;
							end if;
						end loop;
					
						minuto2 := EXTRACT(MINUTE from fila1.fecha);
						hora2 := EXTRACT(HOUR from fila1.fecha);
						dia2 :=  EXTRACT(DAY from fila1.fecha);		-- SI LAS ALERTAS COINCIDEN EN FECHA Y HORA SE HACE UNA DIFERENCIA CON LOS MINUTOS
						mes2 :=  EXTRACT(MONTH from fila1.fecha);
						ano2 :=  EXTRACT(YEAR from fila1.fecha);
						
						if(mes=mes2 and dia=dia2 and hora=hora2 and fila.codigopostal!=fila1.codigopostal ) then
							if ((fila.fecha-fila1.fecha) <='00:05:00'::interval) and (fila.fecha-fila1.fecha) >='00:00:00'::interval then	
								-- SE COMPARA LA DIFERENCIA CON EL INTERVALO DESEADO Y CON 0 PARA EVITAR
								--FECHAS REPETIDAS YA QUE DE LAS FECHAS NEGATIVAS SE TOMA SU MODULO EN
								--EN LA COMPARACION
								return true;													
							end if;														
						end if;
						return false;
					end;
					$$ language plpgsql;`)
if err != nil {
	log.Fatal(err)
}
	_, err = db.Exec(`create or replace function ingresaralertacompra5min() returns void as $$
					declare
					v record;
					c record;
					ingresoalerta boolean;
					intervalo interval;
					esmenor boolean;
					begin
						for v in select * from compra c1 ,comercio c2 where (c1.nrocomercio=c2.nrocomercio )  loop
		
							for c in select * from compra c3 ,comercio c4 where (c3.nrocomercio=c4.nrocomercio )   loop

								if (v.nro_operacion!=c.nro_operacion and v.nrotarjeta=c.nrotarjeta) then		
									--si el nro operacion es distinto (evitar filas iguales) y el nro de tarjeta es igual
									if (v.codigopostal!=c.codigopostal) and (v.nrocomercio!=c.nrocomercio) then	
										-- seguido de un codigo postal distinto y un nro de comercio distinto
										ingresoalerta := validarintervalo5minutos(v,c);	-- se valida el intervalo si ingreso alerta es true 
										if ingresoalerta then
											insert into alerta (nrotarjeta,fecha,nrorechazo,codalerta,descripcion) 
											values(v.nrotarjeta,v.fecha,null,5,'Compra 5 minutos');
										end if;
									end if;
								end if;
							end loop;
						end loop;
					end;
					$$ language plpgsql;`)
	if err != nil {
	log.Fatal(err)
}
	_, err = db.Exec(`create function tiene_dos_rechazos(fila1 record) returns boolean as $$
					declare
					retorno boolean;
					monto_maximo decimal(8,2);
					cantidad_de_rechazos int;

					begin
						retorno=false;

				

						select limitecompra into monto_maximo from tarjeta where (nrotarjeta=fila1.nrotarjeta); -- se busca el limite de compra de la tarjeta


						cantidad_de_rechazos := (select count(*) from rechazo 
						where(fila1.nrotarjeta=nrotarjeta and motivo='supera limite tarjeta' and fila1.fecha=fecha));
					
						if (cantidad_de_rechazos>=2) then
							retorno :=true;
							return retorno;
				
						end if;
				
						return retorno;
					end;
	
					$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}

	_, err = db.Exec(`create or replace function ver_cant_rechazo() returns trigger as $$
					declare
					fila1 record;
					mas_de_dos_rechazos  boolean;
					ya_suspendida boolean;
					dia int;	
					mes int;
					dia1 int;
					mes1 int;
					begin

						mas_de_dos_rechazos := tiene_dos_rechazos(new);
						if (mas_de_dos_rechazos) then

							ya_suspendida = (select estado from tarjeta where (nrotarjeta=new.nrotarjeta))= 'suspendida'; -- si la tarjeta ya esta suspendida no se realiza ningun cambio
		
							if(ya_suspendida=false) then						--sino
								insert into alerta 								-- se inserta la alerta
									(nrotarjeta,fecha,nrorechazo,codalerta,descripcion) 
										values(new.nrotarjeta,CURRENT_TIMESTAMP,null,32,'Tarjeta suspendida. Alcanzado el max de rechazos por dia');
		
								update tarjeta set estado = 'suspendida' where (nrotarjeta=new.nrotarjeta);		-- se cambia sus estado a suspendida
							end if;

						end if;
	
						return new;

					end;

					$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(`create or replace function creartriggers() returns void as $$
					begin
						create trigger alerta_rechazo
						after insert on rechazo
						for each row
						execute procedure ingresaralertarechazo();
						
						
						create trigger vigilar_y_suspender
						after insert on rechazo
						for each row
						execute procedure ver_cant_rechazo();
						
						create trigger vigilar_consumo
						after insert on consumo
						for each row
						execute procedure autorizacion();
						
					end;
						
					$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(`create or replace function revisar_compra() returns void as $$
					begin
						PERFORM ingresarAlertaCompra1min() ;
						PERFORM ingresaralertacompra5min();					
					end;
					$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}
	_, err = db.Exec(`create function resumenprueba() returns void as $$
					begin
						raise notice '=====================================================';
						raise notice 'CASO 1 : CLIENTE COMPRAS EN EL MES';
						PERFORM generarresumen(3,6);
						raise notice '=====================================================';
						raise notice 'CASO 2: Cliente sin compras en el mes';
						PERFORM generarresumen(1,1);
						raise notice '=====================================================';
						raise notice 'CASO 3: Cliente con compras en el mes';
						PERFORM generarresumen(1,6);
						raise notice '=====================================================';
						raise notice 'CASO 4: Cliente no valido';
						PERFORM generarresumen(32,1);
						raise notice '=====================================================';
						raise notice 'CASO 5: Mes no valido';
						PERFORM generarresumen(1,19);
						raise notice '=====================================================';
						raise notice 'CASO 6: Cliente con 2 tarjetas';
						PERFORM generarresumen(20,5);
					end;
					$$ language plpgsql;`)
	if err != nil {
		log.Fatal(err)
	}		
	_, err = db.Exec(`select creartriggers();`)
	if err != nil {
		log.Fatal(err)
	}

}

	
	


func main() {
	db, err := sql.Open("postgres", "user=postgres host=localhost dbname=tarjetadecredito sslmode=disable")
	if err != nil {
		log.Fatal(err)
	}
   	defer db.Close()
	fmt.Printf("Inserte el número\n")
	fmt.Printf("1 - Crear bd\n")
	fmt.Printf("2 - Crear las tablas\n")
	fmt.Printf("3 - Crear PK y FK\n")
	fmt.Printf("4 - Borrar PK y FK\n")
	fmt.Printf("5 - Carga las tablas\n")
	fmt.Printf("6 - Crear sp y trg\n")
	fmt.Printf("7 - Cargar consumos\n")
	fmt.Printf("8 - Cargar cierres de tarjeta\n")
	fmt.Printf("9 - Monitorear Compras\n")
	fmt.Printf("10 - Generar resumenes\n")
	fmt.Printf("11 - Reiniciar BD\n")
	fmt.Printf("12 - Cerrar BD\n")

	var i int
	var flag bool

	for flag == false && i != 12 {
	fmt.Scanf("%d", &i)
		if i == 1 {
			createDatabase()
			fmt.Printf("Se ha conectado a la base de datos\n")
		} else if i == 2 {
			createtables()
			fmt.Printf("Se han creado las tablas\n")
		} else if i == 3 {
			crearPksYFks()
			fmt.Printf("Se han creado las Primary y Foreign Keys\n")
		} else if i == 4 {
			eliminarPksYFks()
			fmt.Printf("Se han borrado las Primary y Foreign Keys\n")
		} else if i == 5 {
			llenartablas()
			fmt.Printf("Se han introducido datos en las tablas\n")
		} else if i == 6 {
			crearStoredProcedure()
			fmt.Printf("Se han creado los store procedures y triggers\n")
		} else if i == 7 {
			insertarConsumos()
			fmt.Printf("Se han cargado los consumos\n")
		}else if i == 8 {
			insertarCierres()
			fmt.Printf("Se han insertado los cierres\n")
		} else if i == 9 {
			_, err := db.Exec(`select revisar_compra();`)
			if err != nil {
				log.Fatal(err)
			  }
			  fmt.Printf("Se ha alertado a los usuarios\n")
		}else if i == 10 {
			_, err := db.Exec(`select resumenprueba();`)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("Se han cargado los resumenes\n")
		} else if i == 11 {
		    db,err := sql.Open("postgres", "user=postgres host=localhost dbname=postgres sslmode=disable")
   			if err != nil {
        		log.Fatal(err)
    		}
    		defer db.Close()
			_, err = db.Exec(`DROP DATABASE tarjetadecredito;`)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Printf("La base de datos fue borrada\n")
		}else if i == 12 {
			defer db.Close()
			flag = false
			fmt.Printf("Gracias por usar la base de datos!\n")
		}
	}
}
